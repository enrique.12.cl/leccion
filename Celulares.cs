public class Celulares : Dispositivos
{
        public Celulares (int imei, string marca, string modelo, string sistema, string fabricacion, string hardware, string funcionalidad1, string funcionalidad2, string funcionalidad3): base(imei, marca,  modelo, sistema, fabricacion, hardware, funcionalidad1, funcionalidad2, funcionalidad3){}

        public void imprimir(){
            Console.WriteLine("Dispositivo a reparar:");
            Console.WriteLine("");
            Console.WriteLine("Marca: "+marca);
            Console.WriteLine("Modelo: "+modelo);
            Console.WriteLine("IMEI: "+imei);
            Console.WriteLine("Sistema Operativo: "+sistema);
            Console.WriteLine("Hardware Adicional: "+hardware);
            Console.WriteLine("------------------");
        }

        public void funcionalidades()
        {
            Console.WriteLine("Funcionalidades: ");
            Console.WriteLine("1.- "+funcionalidad1);
            Console.WriteLine("2.- "+funcionalidad2);
            Console.WriteLine("3.- "+funcionalidad3);
        }
    }